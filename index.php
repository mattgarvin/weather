<?php

//include class defs
require_once('class.weather.php');

//config file, zips we want to process
$zips = require_once('config.php');

//instantiate, load zips from config into object, off to the races
$myweather = new Weather($zips);

//just for fun, lets see the output
//$myweather->output();
