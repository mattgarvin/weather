
<?php

/*
Class to handle query weather and saving to datastore.
Config allows for expanded time retrevial settings (request asked "retrieval frequency should be contained in configuration file",
yet the tool was titled "Hourly Weather Tracker").
Satisfied this req by only saving data once in any 60 minute time span
(multiple queries within the hour wouldn't lead to multiple saves)
*/

class Weather{

	protected $zips_input = false;
	protected $fetched_weather = false;
	protected $fetched_error = false;

	//load weather, save
	function __construct($zip_input){
		//load zips into object
		$this->zips_input = $zip_input;

		//get all weather data from zips
		$this->get_all_weather($this->zips_input);

		//save fetched weather
		$this->save();
	}

	//output fetched data and errors
	public function output(){
		echo '<pre><h2>Weather</h2>';
		print_r($this->fetched_weather);
		if($this->fetched_error){
			echo '</pre><pre><h2>Error</h2>';
			print_r($this->fetched_error);
			echo '</pre>';
		}
	}

	//process array of zips
	private function get_all_weather($zips){
		foreach($zips as $z){
			try {
    			$this->fetched_weather[$z] = $this->get_weather($z);
			} catch (Exception $e) {
				//catch the error
				$this->fetched_error[] = $e->getMessage();
			}
		}
	}

	//retrieve weather from service
	private function get_weather($zip){
		$key = '25ac8fe290a4832810de6b0ab2ba76f6';
		$endpoint = 'http://api.openweathermap.org/data/2.5/weather?';

		$response = @file_get_contents($endpoint.'zip='.$zip.',us&appid='.$key.'&units=imperial');
		if(!$response){
			 throw new Exception('Error: No response for zip '.$zip);
		}
		$weather = json_decode($response); //weather in JSON
		if($weather->cod != '200' ){
			throw new Exception('Error: code '.$weather->cod.' '.$weather->message);
		}
		return $weather;
	}

	//save data to store
	private function save(){
		$db = new Db();
		foreach($this->fetched_weather as $z=>$w){
			//query db - does weather exist for this zip within the hour?
			$checktime = time() - 3600;
			$rows = $db->select("SELECT 'id' FROM 'weather' WHERE zip=".$z." AND dt > ".$checktime);
			//data exists, skip
			if($rows) continue;

			//format result set - some data may not be in result
			$data = $this->format($w);
			$result = $db->query("INSERT INTO weather (zip, weather, pressure, temp, wind_speed, wind_deg, humidity, dt)
			VALUES ('". $z ."', '".$data['weather']."','".$data['pressure']."','".$data['temp']."','".$data['wind_speed']."','".$data['wind_deg']."','".$data['humidity']."','".$data['dt']."')");
		}
	}

	//null data points
	private function format($w){
		$result = array('weather'=>'','pressure'=>'','temp'=>'', 'wind_speed'=> '', 'wind_deg'=>'', 'humidity'=>'', 'dt'=>'');
		if(isset($w->weather[0]->main)){$result['weather']=$w->weather[0]->main;}
		if(isset($w->main->pressure)){$result['pressure']=$w->main->pressure;}
		if(isset($w->main->temp)){$result['temp']=$w->main->temp;}
		if(isset($w->wind->speed)){$result['wind_speed']=$w->wind->speed;}
		if(isset($w->wind->deg)){$result['wind_deg']=$w->wind->deg;}
		if(isset($w->main->humidity)){$result['humidity']=$w->main->humidity;}
		if(isset($w->dt)){$result['dt']=$w->dt;}
		return $result;
	}


} //END class



class Db {

	// connection
    protected static $connection;

    // connect to the database
    public function connect() {
        if(!isset(self::$connection)) {
            // Load configuration
            $config = parse_ini_file('db.ini');
            self::$connection = new mysqli('localhost',$config['username'],$config['password'],$config['dbname']);
        }
        if(self::$connection === false) {
            return false;
        }
        return self::$connection;
    }

    // query the database
    public function query($query) {
        $connection = $this->connect();
        $result = $connection->query($query);
        return $result;
    }

    // fetch rows
    public function select($query) {
        $rows = array();
        $result = $this->query($query);
        if($result === false) {
            return false;
        }
        while ($row = $result -> fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    // fetch the last error
    public function error() {
        $connection = $this->connect();
        return $connection->error;
    }

}
